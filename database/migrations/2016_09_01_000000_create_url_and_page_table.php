<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use LaravelCMF\Content\Support\DatabaseMigrations;

class CreateUrlAndPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cmf_urls', function (Blueprint $table) {
            DatabaseMigrations::urlMigrations($table);
        });

        Schema::create('cmf_pages', function (Blueprint $table) {
            DatabaseMigrations::pageMigrations($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cmf_pages');
        Schema::drop('cmf_urls');
    }
}
