<?php

namespace LaravelCMF\Content\Services;

use LaravelCMF\Base\Resources\Registry;
use LaravelCMF\Content\Models\Eloquent\URL;

class UrlService
{

    public function getUrl($url)
    {
        //return matching URL Resource Model with the matching URL.
        return URL::where('url', '=', $url)->first();
    }

    public function updateChildren($oldUrl, $newUrl)
    {
        //update all URL models that have a prefix matching the old URL to the new URL
        //eg. when a top level URL changes it should update all children.
    }

    public function findResourceByUrl($url)
    {
        $url = $this->getUrl($url);
        if($url) {
            $class = str_replace("\\\\", "\\", $url->item_class);

            if($resource = Registry::instance()->getResourceModelByClass($class)){
                if($extendedResource = $resource->resourceExtends()) {
                    $extendedClass = $extendedResource->getClassName();
                    $resourceModel = $extendedClass::where('url_id', '=', $url->id)->first();
                    $resourceModel = $resourceModel ? $resourceModel->node : $resourceModel;
                } else {
                    $resourceModel = $class::where('url_id', '=', $url->id)->first();
                }

                if(!$resourceModel) return null;
                $resource->setResourceModel($resourceModel);
                return $resource;

            }
        }
        return null;
    }
}