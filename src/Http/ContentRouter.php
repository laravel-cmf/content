<?php

namespace LaravelCMF\Content\Http;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Resources\BaseResource;
use LaravelCMF\Content\Http\Controllers\ContentController;
use LaravelCMF\Content\Services\UrlService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContentRouter
{
    /**
     * @var ContentRouter
     */
    public static $instance;

    /**
     * @var UrlService
     */
    protected $urlService;
    /**
     * @var Router
     */
    private $router;

    /**
     * ContentRouter constructor.
     * @param UrlService $urlService
     * @param Router $router
     */
    public function __construct(UrlService $urlService, Router $router)
    {
        static::$instance = $this;
        $this->urlService = $urlService;
        $this->router = $router;
    }

    /**
     * Reuse the same instance across static calls.
     * @return ContentRouter
     */
    public static function instance()
    {
        if (is_null(static::$instance))
        {
            //Using service container connect the model instance
            app(self::class);
        }
        return static::$instance;
    }

    public function routeToResource(Request $request)
    {
        if($request->getMethod() === 'GET') {
            $urlPath = $request->getPathInfo();

            //remove the initial slash
            if($urlPath === "/") {
                $urlPath = "";
            } else if(strpos($urlPath, "/") === 0){
                $urlPath = substr($urlPath, 1);
            }

            //look for the URL in our database
            $resource = $this->urlService->findResourceByUrl($urlPath);
            if(!$resource) return false;

            $resourceKey = $resource->getResourceKey();


            $action = '@view';
            $controller = $resource->getResourceCmfProperty('frontendController');
            if($controller && class_exists($controller)) {
                $action = $controller.$action;
            } else {
                $action = ContentController::class.$action;
            }

            //make a new route and route to it
            $this->router->model('content_resource', BaseResource::class);
            $this->router->bind('content_resource', function ($value) use ($resource) {
                return $resource;
            });

            $this->router->get('{content_resource}', $action)->where('content_resource', '(.*)');

            //Try routing and if we fail just return the original not found exception
            //Todo implement nice error responses.
            try {
                return $this->router->dispatchToRoute($request);
            } catch (\Exception $e) {

            }

        }

        return false;
    }


    /**
     * Look for a route in this module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|false
     */
    public static function match(Request $request)
    {
        return static::instance()->routeToResource($request);
    }
}