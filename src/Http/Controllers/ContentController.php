<?php

namespace LaravelCMF\Content\Http\Controllers;

use Illuminate\Routing\Controller;
use LaravelCMF\Base\Resources\BaseResource;

class ContentController extends Controller
{
    public function view(BaseResource $resource)
    {
        $view = CMFTemplate("content.view");
        $resourceKey = $resource->getResourceKey();
        $template = $resource->template() ? $resource->template() : "$resourceKey.view";
        if(view()->exists($template)) {
            $view = $template;
        }

        return response()->view($view, ['model' => $resource->getResourceModel()], 200);
    }
}