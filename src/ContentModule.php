<?php

namespace LaravelCMF\Content;

use Illuminate\Routing\Router;
use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Modules\ModuleInterface;
use LaravelCMF\Base\Providers\CMFProvider;

class ContentModule implements ModuleInterface
{


    public function getModuleName()
    {
        return 'content';
    }

    public function boot(CMFProvider $serviceProvider)
    {
        $serviceProvider->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');

        $serviceProvider->loadViewsFrom(
            __DIR__ . '/../resources/views',
            CMF::PACKAGE_NAME);
    }

    public function register(CMFProvider $serviceProvider)
    {

    }

    public function mapRoutes(Router $router)
    {
    }

    public function getAssets()
    {
        // TODO: Implement getAssets() method.
    }

    public function getConfig()
    {
        return require __DIR__ . '/../config/module.php';
    }
}