<?php

namespace LaravelCMF\Content\Support;

use Illuminate\Database\Schema\Blueprint;
use LaravelCMF\Base\Support\BaseDatabaseMigrations;

class DatabaseMigrations
{

    public static function pageMigrations(Blueprint $table)
    {
        $table->increments('id');
        $table->unsignedInteger('creator_id');
        $table->unsignedInteger('url_id');
        $table->boolean('is_visible');
        $table->integer('pos')->nullable();
        $table->string('menu_title')->max(255);
        $table->string('title')->max(255);
        $table->string('html_title')->max(255);
        $table->text('meta')->nullable();
        $table->text('content')->nullable();
        $table->text('settings')->nullable();
        $table->foreign('url_id')->references('id')->on('cmf_urls')
            ->onUpdate('cascade')->onDelete('cascade');

        $table->timestamps();
    }

    public static function pageNodeMigrations(Blueprint $table)
    {
        BaseDatabaseMigrations::nodeMigrations($table);
        $table->unsignedInteger('creator_id');
        $table->unsignedInteger('url_id');
        $table->boolean('is_visible');
        $table->integer('pos')->nullable();
        $table->string('menu_title')->max(255);
        $table->string('html_title')->max(255);
        $table->text('meta')->nullable();
        $table->text('content')->nullable();
        $table->text('settings')->nullable();
        $table->foreign('url_id')->references('id')->on('cmf_urls')
            ->onUpdate('cascade')->onDelete('cascade');
    }

    public static function urlMigrations(Blueprint $table)
    {
        $table->increments('id');
        $table->unsignedInteger('item_identifier')->nullable();
        $table->string('item_class')->nullable()->max(255);
        $table->string('url')->unique()->max(255);
        $table->string('slug')->max(255);
        $table->string('prefix')->max(255)->nullable();
        $table->unsignedInteger('alias')->nullable();
        $table->foreign('alias')->references('id')->on('cmf_urls');
        $table->text('settings')->nullable();
        $table->timestamps();
    }
}