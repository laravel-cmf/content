<?php

namespace LaravelCMF\Content\Models\Eloquent;

use LaravelCMF\Base\Models\Eloquent\BaseNode;

abstract class PageNode extends BaseNode
{
    public static $_icon = 'fa-file-text-o';
    public static $_baseFields = [
        'title',
        'parent',
        'is_visible',
        'url',
        'menu_title',
        'html_title',
        'content',
        'meta',
        'settings'
    ];
    public static $_fields = [
        'is_visible' => ['field' => 'checkbox', 'default' => true, 'group' => 'title'],
        'title' => ['group' => 'title', 'after' => 'visible'],
        'menu_title' => [
            'after' => 'title',
            'group' => 'title',
            'field' => 'template',
            'template' => "model.title"
        ],
        'url' => [
            'group' => 'title',
            'field' => 'url',
            //'prefix' => ''//'{{date:Y-m-d}}/{{date:m}}' //prefix (in addition to any parent URL)
            'description' => 'This is the URL which by default is automatically generated.'
        ],
        'html_title' => [
            'group' => 'meta',
            'field' => 'template',
            'template' => 'model.title'
        ],
        'content' => [
            'group' => 'main',
            'field' => 'richtext'
        ],
        'meta' => [
            'field' => 'object',
            'fields' => [
                'description' => [
                    'field' => 'template',
                    'template' => 'model.title'
                ],
            ],
            'dynamic' => true,
            'group' => 'meta',
            'widget' => false
        ],
        'settings' => [
            'field' => 'settings'
        ]
    ];

    public static $_listFields = ['title', 'url'];

    public static $_groups = [
        'title' => ['title' => 'Title & URLS', 'icon' => 'tag'],
        'meta' => ['title' => 'Meta Data (SEO)', 'icon' => 'globe', 'tab' => 'meta'],
        'main' => ['tab' => 'content']
    ];

    public static $_tabs = [
        'main' => 'Main',
        'meta' => 'Meta Data (SEO)',
        'content' => 'Content'
    ];

    public static $_defaultGroup = 'main';

    public static $_slugFields = ['menu_title'];

    public function setUrlAttribute($value)
    {
        return $this->setAttribute('url_id', $value->id);
    }
    public function getUrlAttribute()
    {
        return $this->getAttribute('url_id');
    }

}