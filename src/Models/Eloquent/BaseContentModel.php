<?php

namespace LaravelCMF\Content\Models\Eloquent;

use LaravelCMF\Base\Models\Eloquent\Administrator;
use LaravelCMF\Base\Models\Eloquent\BaseModel;

abstract class BaseContentModel extends BaseModel
{
    public static $_fields = [
    ];

    /**
     * The identifier
     * @var integer
     */
    protected $id;

    /**
     * The Admin creator of this page
     * @var integer
     **/
    protected $creator_id;

    /**
     * The URL of this page
     * @var integer
     **/
    protected $url_id;

    /**
     * @return URL
     */
    public function url()
    {
        return $this->belongsTo(URL::class);
    }

}