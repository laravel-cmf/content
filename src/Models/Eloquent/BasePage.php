<?php

namespace LaravelCMF\Content\Models\Eloquent;

abstract class BasePage extends BaseContentModel
{
    //public static $_frontendController = '\App\Http\Controllers\Controller';

    public static $_fields = [
        'is_visible' => ['field' => 'checkbox', 'default' => true, 'group' => 'title'],
        'title' => ['group' => 'title', 'after' => 'visible'],
        'menu_title' => [
            'after' => 'title',
            'group' => 'title',
            'field' => 'template',
            'template' => "model.title"
        ],
        'url' => [
            'group' => 'title',
            'field' => 'url',
            //'prefix' => ''//'{{date:Y-m-d}}/{{date:m}}' //prefix (in addition to any parent URL)
            'description' => 'This is the URL which by default is automatically generated.'
        ],
        'html_title' => [
            'group' => 'meta',
            'field' => 'template',
            'template' => 'model.title'
        ],
        'content' => [
            'group' => 'main',
            'field' => 'richtext'
        ],
        'meta' => [
            'field' => 'object',
            'fields' => [
                'description' => [
                    'field' => 'template',
                    'template' => 'model.title'
                ],
            ],
            'dynamic' => true,
            'group' => 'meta',
            'widget' => false
        ],
        'settings' => [
            'field' => 'settings'
        ]
    ];

    public static $_listFields = ['title', 'url'];

    public static $_groups = [
        'title' => ['title' => 'Title & URLS', 'icon' => 'tag'],
        'meta' => ['title' => 'Meta Data (SEO)', 'icon' => 'globe', 'tab' => 'meta'],
        'main' => ['tab' => 'content']
    ];

    public static $_tabs = [
        'main' => 'Main',
        'meta' => 'Meta Data (SEO)',
        'content' => 'Content'
    ];

    public static $_defaultGroup = 'main';

    public static $_slugFields = ['menu_title'];

    /** inheritdoc */
    public function blank($ignore_fields = null)
    {
        parent::blank($ignore_fields);
        $this_class  = get_class($this);
        $this->title = $this->menu_title = $this->html_title = $this_class::singular();
    }

    /**
     * Dictates if the page is visible in navigations etc.
     * @var bool
     */
    protected $is_visible = true;

    /**
     * Title to show in navigations etc.
     * @var string
     **/
    protected $menu_title;

    /**
     * Page Title
     * @var string
     **/
    protected $title;

    /**
     * HTML Title
     * @var string
     **/
    protected $html_title;

    /**
     * Meta description for this page
     * @var string
     **/
    protected $meta_description;

    /**
     * Rich text containing the page content
     * @var string
     */
    protected $content;

    /**
     * Extra meta data such as OG tags
     * @var array
     */
    protected $extra_meta = [];

    public function setUrlAttribute($value)
    {
        return $this->attributes['url_id'] = $value->id;
    }
}