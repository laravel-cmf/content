<?php

namespace LaravelCMF\Content\Models\Eloquent;

use LaravelCMF\Base\Models\Eloquent\BaseModel;

class URL extends BaseModel
{
    protected $table = 'cmf_urls';
    public static $_resourceKey = 'urls';

    public static $_fields = [
        'item_identifier' => ['visible' => false],
        'item_class' => ['visible' => false],
        'url' => [
            'visible' => false,
            'type' => 'concat',
            'fields' => ['prefix','slug'],
            'separator' => '/',
        ],
        'prefix' => [
            'visible' => false
        ],
        'slug' => [
            'prepend' => 'prefix'
        ],
        'settings' => []
    ];

    /**
     * The item the URL references
     * @var integer
     */
    protected $item_identifier;

    /**
     * The Class the item id references
     * @var string
     */
    protected $item_class = '';

    /**
     * The actual URL including any parent URL slugs.
     * @var string
     */
    protected $url;

    /**
     * The slug for this part of the full URL
     * @var string
     */
    protected $slug;

    /**
     * The prefix to the slug combined to make the full URL.
     * @var string|null
     */
    protected $prefix;

    /**
     * The actual URL reference.
     * @var URL|null
     */
    protected $alias;

    /**
     * Any other URLs that reference this one
     * @var URL[]|null
     */
    protected $aliases;

    /**
     * Settings stored
     * @var array
     */
    protected $settings = [];

    public function getSetting($key, $default = null)
    {
        return isset($this->settings[$key]) ? $this->settings[$key] : $default;
    }

    public function getSettingsAttribute()
    {
        return isset($this->attributes['settings']) ? json_decode($this->attributes['settings'], true) : null;
    }

}