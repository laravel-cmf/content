<div id="{{$field->getFieldId()}}" class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label class="col-lg-2 control-label">URL</label>
    <div class="col-lg-10">
        @if($resourceField->value()->exists)
        <input type="hidden" name="{{$field->getField('item_identifier')->getFieldName()}}" value="{{$field->getField('prefix')->getResourceField()->value()}}" />
        @endif
        <auto-update-field name="{{$field->getFieldName()}}" prefix="{{$resourceField->getPrefix()}}" :value.sync="model.url.slug" :template-value="model.title | slug" :settings.sync="model.url.settings"></auto-update-field>
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>
