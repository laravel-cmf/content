<?php
use LaravelCMF\Content\Resources\Fields;
use LaravelCMF\Content\Models\Eloquent;


return [
    'models' => [
        Eloquent\BasePage::class,
        Eloquent\URL::class,
    ],
    'resource-fields' => [
        'url' => Fields\Relation\URL::class
    ]
];